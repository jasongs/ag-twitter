
'use strict';


module.exports = function(app) {
    var Controllers = require('../controllers/ag-tweet-controller');
    var mongoose = require('mongoose');

    app.route('/adduser')
        .post(function (req, res) {
            Controllers.createuser(req, res);

        });

    app.route('/login')
        .post(
            function (req, res) {
                Controllers.login(req, res);
            });
    app.route('/feed/:you/:id')
        .get(function (req, res) {

                Controllers.feed(req,res);

    });
    app.route('/userlist')
        .get(function (req, res) {
            Controllers.userslist(req,res);

        });
    app.route('/follow/:you/:user')
        .get(
            function(req,res) {
                Controllers.follow(req,res);
            }
        )
    app.route('/unfollow/:you/:user')
        .get(
            function(req,res) {
                Controllers.unfollow(req,res);
            }
        )
    app.route('/tweet/:you')
        .post(
            function (req, res) {
                Controllers.tweet(req, res);
            });
    app.use(function(req, res, next) {
        var err = new Error('Not Found');
        return res.status(404).json({
                message: 'Not found',
                err: err
            });
        
        next(err);
    });
};