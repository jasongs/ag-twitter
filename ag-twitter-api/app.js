
var express = require('express'),
    compression = require('compression'),    
    app = express(),        
    port = process.env.PORT || 3001;

var EasyPbkdf2 = require("easy-pbkdf2");

mongoose = require('mongoose');
usermodel = require('./models/users')
tweetmodel = require('./models/tweets')
bodyParser = require('body-parser');

var morgan = require('morgan');




mongoose.Promise = global.Promise;

var pbkdf2_options = {
    // default DEFAULT_HASH_ITERATIONS is 512
    "DEFAULT_HASH_ITERATIONS": 10000,
    // default SALT_SIZE is 32
    "SALT_SIZE": 32,
    // default KEY_LENGTH is 256
    "KEY_LENGTH": 128
};

easyPbkdf2 = new EasyPbkdf2(pbkdf2_options);

var dbURI = 'mongodb://duckeggdigital.com/agtwitter';

var db = mongoose.connection;
db.on('connecting', function() {
    console.log('connecting');
});

db.on('error', function(error) {
    console.log('Error in MongoDb connection: ' + error);
    mongoose.disconnect();
});
db.on('connected', function() {
    console.log('connected!');
});
db.once('open', function() {
    console.log('connection open');
});
db.on('reconnected', function() {
    console.log('reconnected');
});
db.on('timeout', function() {
    console.log('timeout!');
});
db.on('disconnected', function() {
    console.log('disconnected');
    console.log('dbURI is: ' + dbURI);
    mongoose.connect(dbURI, {
        useMongoClient: true,
        keepAlive: 1,
        connectTimeoutMS: 30000,
    }).catch(function(err) {
        console.log("HERE");
    });
});
console.log('dbURI is: ' + dbURI);
process.on('unhandledRejection', function(error) {
    console.log('unhandledRejection', error.message);
});
mongoose.connect(dbURI, {
    useMongoClient: true,
    keepAlive: 1,
    connectTimeoutMS: 30000
});
app.use(compression());




app.set('superSecret', 'dddsefgfg');
app.set('salt', easyPbkdf2.generateSalt());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());


app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');

var routes = require('./routes/agTwitterRoutes');
routes(app);
app.use('/', routes);
app.use(morgan('dev'));




var server = app.listen(port);
server.timeout = 2400000;

console.log('RESTful API server started on: ' + port + " timeout of " + server.timeout);

