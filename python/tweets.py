

# get users
users = {}
f = open("user.txt","r")
data = f.read()
f.close()

for line in data.split("\n"):
    if " follows " in line:
        user,follows = line.split(" follows ")
        if not users.has_key(user):
            users[user] =  []
        for followed in follows.split(","):
            followed = followed.strip()
            if not users.has_key(followed):
                users[followed] =  []
            if followed not in users[user]:
                users[user].append(followed)

# go through tweets
f = open("tweet.txt","r")
data = f.read()
f.close()

twitterfeed = {}
for user in sorted(users.keys()):
    if not twitterfeed.has_key(user):
        twitterfeed[user] =  []
    follows = users[user]
    for line in data.split("\n"):
        if "> " in line:
            author,tweet = line.split("> ")
            tweet = tweet.strip()
            if author == user or author in follows:
                if len(twitterfeed[user]) < 1:
                    item = {}
                    item['author'] = author
                    item['tweet'] = tweet
                    twitterfeed[user].append(item)

                for i in twitterfeed[user]:

                    set1 = set(tweet.lower().split(' '))
                    set2 = set(i['tweet'].strip().lower().split(' '))

                    if sorted(set1) == sorted(set2):
                        pass
                    else:
                        item = {}
                        item['author'] = author
                        item['tweet'] = tweet
                        twitterfeed[user].append(item)


for user in sorted(twitterfeed.keys()):
    print "%s\n" % user
    lines = []
    for tw in twitterfeed[user]:
        line = "@%s: %s" % (tw['author'], tw['tweet'])
        if line not in lines:
            print line
            lines.append(line)
    print "\n"
