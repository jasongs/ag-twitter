let mongoose = require('mongoose'); 
let autoIncrement = require('mongoose-auto-increment');
let deepPopulate = require('mongoose-deep-populate')(mongoose);
let Schema = mongoose.Schema;
let moment = require('moment');
autoIncrement.initialize(mongoose.connection);


let User = mongoose.model('User');

let TweetSchema = new Schema({
	user: {type: Number, ref: 'User'},
	tweet: {
		type: String,
        required: true
	},
	when: { type: Date}
});
TweetSchema.plugin(autoIncrement.plugin, 'TweetSchemaCounter');
TweetSchema.plugin(deepPopulate);

TweetSchema.pre('save', function(next) {

    var tweet = this;
    console.log(this.when);
    if (this.when === undefined || this.when === null) {

        tweet.when = moment.utc(new Date());
        console.log(tweet.when);
        return next();
    }
    else {
        return next();
    }
});

let TweetStreamSchema = new Schema({
	tweet: {type: Number, ref: 'Tweet'},
	interested: [{type: Number, ref: 'User'}]

}, { capped: { size: 1024, max: 1000, autoIndexId: true } });
TweetStreamSchema.plugin(autoIncrement.plugin, 'TweetStreamSchemaCounter');

let Tweet = mongoose.model("Tweet", TweetSchema);
let TweetStream = mongoose.model("TweetStream", TweetStreamSchema);

module.exports = Tweet,TweetStream;