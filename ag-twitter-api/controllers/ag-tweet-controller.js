'use strict';
var mongoose = require('mongoose'),
User = mongoose.model('User'),
Tweet = mongoose.model('Tweet');
const moment = require('moment');
const JSONStream = require('JSONStream');

exports.login = function (req,res) {

	User.findOne({'handle': req.body.handle}, function (err, user) {
        if (err) {
            console.log("error: " + err);
            res.send(err);
        }
        if (!user) {
            console.log('Invalid handle and/or password');
            return res.status(403).json({
                message: 'Invalid handle and/or password'
            });
        }
        user.comparePassword(req.body.password, function (err, isMatch) {
            if (isMatch && !err) {
                console.log(user.profile.firstName + " " + user.profile.lastName + " logged in");
                res.send(user);
            } else {
                if (!isMatch) {
                    
                    console.log('Invalid handle and/or password');
                    return res.status(403).json({
                        message: 'Invalid handle and/or password'
                    });
                }
                else {
                	console.log("error: " + err);
                    res.send(err);
                }
            }
        });

    });
};

exports.createuser = function(req,res) {

    if (req.body.hasOwnProperty('handle') !== true) {
        return res.status(400).json({
                message: 'handle is required'
            });
    }
    if (req.body.hasOwnProperty('password') !== true) {
        return res.status(400).json({
                message: 'password is required'
            });
    }

    var user = new User();
    user.handle = req.body.handle;
    user.password = req.body.password;
    if (req.body.hasOwnProperty('profile')) {
        var profile = req.body.profile;
        if (profile.hasOwnProperty('firstName')) {
            user.profile.firstName = profile.firstName;       
        }
        if (profile.hasOwnProperty('lastName')) {
            user.profile.lastName = profile.lastName;       
        }
    }
    user.save(function (err, user_saved) {
        if (err) {
            if (err.errmsg.includes("duplicate")) {
                return res.status(403).json({
                    message: 'handle \'' + user.handle + '\' is already taken'
                });
            }
            else {
                return res.status(500).json({
                message: err.errmsg
            
            });
            }
        }
        res.send(user_saved);
    });
}

exports.follow  = function(req,res) {
    User.findOne({_id:req.params.you}, function (err,found_you){
            if (err) {
                return res.status(500).json({
                    message: err.errmsg
                });
            }
            else {
                if (found_you === null) {
                    return res.status(200).json({
                        success: false,
                        message: "Your user was not found"
                    });
                }
                else {
                    var option = {};
                    if (Number.isInteger(req.params.user)) {
                        option._id = req.params.user;
                    }
                    else {
                        option.handle = req.params.user;
                    }
                    User.findOne(option, function (err, following) {
                        if (err) {
                            return res.status(500).json({
                                message: err.errmsg
                            });
                        }
                        else {
                            if (following === null) {
                                return res.status(200).json({
                                    success: false,
                                    message: "User to follow was not found"
                                });
                            }
                            else {
                                if (found_you.following.includes(following._id)) {
                                    return res.status(200).json({
                                        message: "You are already following \'" + following.handle + "\'"
                                    });
                                }
                                else {
                                    found_you.following.push(following._id);
                                    found_you.save(function (err, found_you_saved) {
                                        if (err) {
                                            return res.status(500).json({
                                                message: "Failed to save following: " + err.errmsg
                                            });
                                        }
                                        else {
                                            following.followers.push(found_you._id);
                                            following.save(function (err, following_saved) {
                                                if (err) {
                                                    return res.status(500).json({
                                                        message: "Failed to save followers: " + err.errmsg
                                                    });
                                                }
                                                else {
                                                    return res.status(200).json({
                                                        message: "You are now following \'" + following.handle + "\'"
                                                    });
                                                }
                                            });
                                        }
                                    });
                                }
                            }
                        }
                    });
                }
            }
    });
}

exports.unfollow  = function(req,res) {
    User.findOne({_id:req.params.you}, function (err,found_you){
        if (err) {
            return res.status(500).json({
                message: err.errmsg
            });
        }
        else {
            if (found_you === null) {
                return res.status(200).json({
                    success: false,
                    message: "Your user was not found"
                });
            }
            else {
                var option = {};
                if (Number.isInteger(req.params.user)) {
                    option._id = req.params.user;
                }
                else {
                    option.handle = req.params.user;
                }
                User.findOne(option, function (err, following) {
                    if (err) {
                        return res.status(500).json({
                            message: err.errmsg
                        });
                    }
                    else {
                        if (following === null) {
                            return res.status(200).json({
                                success: false,
                                message: "User to unfollow was not found"
                            });
                        }
                        else {
                            if (found_you.following.includes(following._id) !== true) {
                                return res.status(200).json({
                                    message: "You are not following \'" + following.handle + "\'"
                                });
                            }
                            else {
                                var index = found_you.following.indexOf(following._id);
                                found_you.following.splice(index, 1);
                                found_you.save(function (err, found_you_saved) {
                                    if (err) {
                                        return res.status(500).json({
                                            message: "Failed to save unfollowing: " + err.errmsg
                                        });
                                    }
                                    else {
                                        var index = following.followers.indexOf(following._id);
                                        following.followers.splice(index, 1);

                                        following.save(function (err, following_saved) {
                                            if (err) {
                                                return res.status(500).json({
                                                    message: "Failed to save followers: " + err.errmsg
                                                });
                                            }
                                            else {
                                                return res.status(200).json({
                                                    message: "You are no longer following \'" + following.handle + "\'"
                                                });
                                            }
                                        });
                                    }
                                });
                            }
                        }
                    }
                });
            }
        }
    });
}

exports.feed = function(req,res) {
    User.findOne({_id: req.params.you}, function (err, found_you) {
        if (err) {
            return res.status(500).json({
                message: err.errmsg
            });
        }
        else {
            if (found_you === null) {
                return res.status(200).json({
                    success: false,
                    message: "Your user was not found"
                });
            }
            else {
                console.log("We hjere");
                let which_users = found_you.following;
                which_users.push(found_you._id);
                console.log(which_users);
                let option = {'user': {'$in': which_users},
                        '_id':{ $gt:req.params.id }};
                Tweet.find(option)
                    .sort('+when')
                    .populate('user')
                    .cursor()
                    .pipe(JSONStream.stringify())
                    .pipe(res.type('json'))
            }
        }
    });
}


exports.userslist = function(req,res) {
    var response = [];
    User.find({},{_id: 1, handle: 1, following: 1}).deepPopulate('following').exec(function(err,allusers) {

        allusers.forEach(function(this_user) {
           let imfollowing = [];
           this_user.following.forEach(function(follows) {
               imfollowing.push(follows.handle + " (" + follows._id +")");
           });
           response.push(this_user.handle + " (" + this_user._id +") following: [" + imfollowing + "]" );
       }) ;

       res.send(response);
    });
};

exports.tweet  = function(req,res) {
    if (req.body.hasOwnProperty("tweet") !== true) {
        return res.status(400).json({
            message: "Tweet is required"
        });
    }
    else {

        User.findOne({_id: req.params.you}, function (err, found_you) {
            if (err) {
                return res.status(500).json({
                    message: err.errmsg
                });
            }
            else {
                if (found_you === null) {
                    return res.status(200).json({
                        success: false,
                        message: "Your user was not found"
                    });
                }
                else {

                    var tweet = new Tweet();
                    tweet.user = found_you._id;
                    tweet.tweet = req.body.tweet;
                    tweet.save(function (err, tweet_saved) {
                        if (err) {
                            return res.status(500).json({
                                message: "Failed to save tweet: " + err.errmsg
                            });
                        }
                        else {

                            return res.status(200).json({
                                message: "Tweet saved"
                            });
                        }
                    });
                }
            }
        });
    }
}