# README #

Allan Gray interview project: twitter like system

### What is this repository for? ###

* Consists of an API node js , express , mongodb and a command line interface
* Version 1.0.0

I have only run this on Mac and am sure linux will work too but cant vouch for on windows.

Assumes you have both node and npm installed and I used node version 8.1.0

### How do I get set up? ###

* in first terminal window
* cd into ag-twitter-api
* npm install
* npm start
* Database configuration: its using a remote mongodb I setup

* in second terminal window
* cd into ag-twitter-cli
* npm install
* npm link
* ag-twitter-cli --help
* This will give you the usage instructions below
* commands have full command name or a short alias eg addUser or a, login or l etc
* you need to login for the commands to work in most cases: eg. ag-twitter-cli login or ag-twitter-cli l alan password
* ive set all current users passwords to password


Usage: ag-twitter-cli [options] [command]

  Allan Gray twitter like system


  Options:

    -V, --version  output the version number
    -h, --help     output usage information


  Commands:

    addUser|a <firstname> <lastname> <handle> <password>  Add a user
    login|l <handle> <password>                           Login a user
    logout|x                                              Logout a user
    feed|fd                                               User feed
    follow|fw <user>                                      Follow a user by their id or handle
    unfollow|uf <user>                                    UnFollow a user by their id or handle
    tweet|t <message>                                     Tweet a message (144 chars)
    userlist|ul                                           List all users