'use strict';
var mongoose = require('mongoose'); 
var autoIncrement = require('mongoose-auto-increment');
var deepPopulate = require('mongoose-deep-populate')(mongoose);
var EasyPbkdf2 = require('easy-pbkdf2');
var Schema = mongoose.Schema;
autoIncrement.initialize(mongoose.connection);


var UserSchema = new Schema({
    handle: {
        type: String,
        lowercase: true,
        unique: true,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    salt: String,    
    profile: {
        firstName: { type: String },
        lastName: { type: String }
    },
    followers: [{type: Number, ref: 'User'}],
    following: [{type: Number, ref: 'User'}],
    lastlogin: { type: Date, default: +new Date() },
});

UserSchema.pre('save', function(next) {

    var user = this;
    if (this.isModified('password') || this.isNew) {
        console.log("before hash");        
        easyPbkdf2.secureHash(user.password, function(err, encryptedPassword, originalSalt) {

            if (err) {
                console.log("err hashing " + err);
                return next(err);
            }            
            user.password = encryptedPassword;
            user.salt = originalSalt;
            console.log("user.salt : " + user.salt);
            next();
        });
    } else {
        return next();
    }
});

UserSchema.methods.comparePassword = function(passw, cb) {
    console.log(passw);
    easyPbkdf2.verify(this.salt, this.password, passw, function(err, isMatch) {
        if (err) {
            return cb(err);
        }
        cb(null, isMatch);
    });
    
};
UserSchema.plugin(autoIncrement.plugin, 'UserSchemaCounter')
UserSchema.plugin(deepPopulate);

var User = mongoose.model("User", UserSchema);

module.exports = User;