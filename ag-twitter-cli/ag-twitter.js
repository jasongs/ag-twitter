#!/usr/bin/env node

const program = require('commander');

const {login,createuser, feed, logout, follow,unfollow,tweet, userlist}  = require('./logic');



program
  .version('0.0.1')
  .description('Allan Gray twitter like system');


program
    .command('addUser <firstname> <lastname> <handle> <password>')
    .alias('a')
    .description('Add a user')
    .action((firstname, lastname, handle, password) => {

        createuser(handle, password, {"firstName": firstname, "lastName": lastname });
});


program
    .command('login <handle> <password>')
    .alias('l')
    .description('Login a user')
    .action((handle, password) => {

        login(handle, password);
    });

program
    .command('logout')
    .alias('x')
    .description('Logout a user')
    .action(() => {

        logout();
    });

program
    .command('feed ')
    .alias('fd')
    .description('User feed')
    .action(() => {

        feed();
    });

program
    .command('follow <user>')
    .alias('fw')
    .description('Follow a user by their id or handle')
    .action((user) => {

        follow(user);
    });

program
    .command('unfollow <user>')
    .alias('uf')
    .description('UnFollow a user by their id or handle')
    .action((user) => {

        unfollow(user);
    });

program
    .command('tweet <message>')
    .alias('t')
    .description('Tweet a message (144 chars)')
    .action((message) => {

        tweet(message);
    });


program
    .command('userlist ')
    .alias('ul')
    .description('List all users')
    .action(() => {

        userlist();
    });
program.parse(process.argv);