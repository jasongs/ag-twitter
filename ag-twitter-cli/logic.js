'use strict';
const request = require('request');
const baseUrl = "http://localhost:3001";
const Cache = require('async-disk-cache');
const cache = new Cache('my-cache', {
    compression: 'gzip' | 'deflate' | 'deflateRaw', // basically just what nodes zlib's ships with
    supportBuffer: 'true' | 'false' // add support for file caching (default `false`)
})

const moment = require('moment');

const createuser = (handle,password, profile ) => {
    cache.has('user').then(function(wasFound) {
        if (!wasFound) {
            console.log("You need to Login first");
        }
        else {
            const requestData = {
                "handle": handle,
                "password": password,
                "profile": {
                    "firstName": profile.firstName,
                    "lastName": profile.lastName
                }
            }
            request({
                url: baseUrl + "/adduser",
                method: "POST",
                headers: {
                    "content-type": "application/json",
                },
                json: requestData
            }, function (err, res, body) {
                if (res.statusCode !== 200) {
                    console.log(body.message);
                }
                else {
                    console.log(body);

                }
            });
        }
    });
};

const login = (handle,password) => {

    const requestData = {
        "handle": handle,
        "password": password,

    }
    request({
        url: baseUrl + "/login",
        method: "POST",
        headers: {
            "content-type": "application/json",
        },
        json: requestData
    }, function (err, res, body) {
        if (res.statusCode !== 200) {
            console.log(body.message);
        }
        else {
            console.log(body);
            cache.has('user').then(function(wasFound) {
                if (wasFound) {
                    cache.remove('user').then(function() {
                        cache.set('user', JSON.stringify(body)).then(function() {

                        });
                    })
                }
                else {
                    cache.set('user', JSON.stringify(body)).then(function() {

                    });
                }
            });

        }
    });

};


function getFeed(userid,fromid) {

    let body = '';

    let url = baseUrl + "/feed/" + userid;
    if (fromid === null || fromid === undefined || fromid === 'undefined') {

        fromid = 0;

    }
    url = url + "/" + fromid;

    request
        .get(url)
        .on('data', function(response) {

            body += response
        })
        .on('end', function(){

            let data = JSON.parse(body);
            if (data.length > 0) {
                data.forEach(function (tweet) {
                    console.log("@" +tweet.user.handle + ": " + tweet.tweet + " (" + moment.utc(tweet.when).format("YYYY-MM-DD HH:mm") + ")");
                    if (fromid < tweet._id) {
                        fromid = tweet._id
                    }
                });
            }
            setTimeout(function () {
                getFeed(userid,fromid);
            }, 3000);
        });
}

const feed = () => {
    cache.has('user').then(function(wasFound) {
        if (!wasFound) {
            console.log("You need to Login first");
        }
        else {
            cache.get('user').then(function(cacheEntry) {
                let userData = JSON.parse(cacheEntry.value);
                console.log("Twitter feed " + userData.handle + " (press ctrl c to break out)");
                getFeed(userData._id,null);
            });
         }
    });

}

const logout = () => {
    cache.remove('user').then(function() {
        console.log("Logged out");
    });
}

const follow = (user) => {

    cache.has('user').then(function(wasFound) {

        if (!wasFound) {
            console.log("You need to Login first");
        }
        else {
            cache.get('user').then(function(cacheEntry) {
                var your_id = JSON.parse(cacheEntry.value)._id;
                const url = baseUrl + "/follow/" + your_id + "/" + user;
                request(url, function (error, response, body) {
                    if (error) {
                        console.log('error:', error);
                    }
                    else {

                        console.log(JSON.parse(body).message);
                    }
                });

            });
        }
    });
}

const unfollow = (user) => {

    cache.has('user').then(function(wasFound) {

        if (!wasFound) {
            console.log("You need to Login first");
        }
        else {
            cache.get('user').then(function(cacheEntry) {
                var your_id = JSON.parse(cacheEntry.value)._id;
                const url = baseUrl + "/unfollow/" + your_id + "/" + user;
                request(url, function (error, response, body) {
                    if (error) {
                        console.log('error:', error);
                    }
                    else {

                        console.log(JSON.parse(body).message);
                    }
                });

            });
        }
    });
}


const tweet = (msg) => {

    cache.has('user').then(function(wasFound) {

        if (!wasFound) {
            console.log("You need to Login first");
        }
        else {
            cache.get('user').then(function(cacheEntry) {
                var your_id = JSON.parse(cacheEntry.value)._id;
                const url = baseUrl + "/tweet/" + your_id;

                if (msg.length > 144) {
                    console.log("Tweet is too long");
                }
                else {
                    var requestData = {
                        "tweet": msg
                    };
                    request({
                        url: url,
                        method: "POST",
                        headers: {
                            "content-type": "application/json",
                        },
                        json: requestData
                    }, function (err, res, body) {
                        console.log(body.message);
                    });
                }

            });
        }
    });
}

const userlist = () => {
    cache.has('user').then(function(wasFound) {

        if (!wasFound) {
            console.log("You need to Login first");
        }
        else {
            const url = baseUrl + "/userlist";
            request(url, function (error, response, body) {
                if (error) {
                    console.log('error:', error);
                }
                else {

                    console.log(JSON.parse(body));
                }
            });
        }
    });
}
module.exports = {login, createuser, feed, logout, follow, unfollow, tweet, userlist};